import java.util.Scanner;

public class Averager
{
    
    public static void main(String[] args)
    {
        //Data 
        //create a scanner object
        Scanner input = new Scanner(System.in);
        //sum of grades variable
        int sum = 0;
        //quiz count variable
        int count = 0;
        //average variable (fractional)
        double average;
        
        //Processing
        
        //while the current quiz count is less than 10 
        while (count < 2)
        {
            //Prompt user for grade
            System.out.print("Enter grade: ");
            //Get grade from user
            int grade = input.nextInt();
            //add 1 to count variable
            count = count + 1;
            //add the current grade to the sum
            sum = sum + grade;
        }
            
        //calculate average using the count and sum variable
        average = (double)(sum) / count;
        //Report to user the resul
        System.out.printf("The class average is %f \n", average);
    }
}

