import java.util.Scanner;
public class SentinelAverager
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        // Initialize sum to zero
        double sum = 0;
        // Initialize count to zero
        int count = 0;
        // declare average
        double average;
        
         
        // Prompt the user for grade
        System.out.print("Enter grade or -1 to end: ");
        // Get and store the grade from user
        int grade = input.nextInt();
        //while grade is not -1 
        while (grade != -1)
        {
            //   Prompt the user for grade
            System.out.print("Enter grade: ");
            //   Get and store the grade from user
            grade = input.nextInt();
            
            if (grade != -1)
            {
                //   add 1 to count
                count += 1;
                //   add grade to sum
                sum += grade;
            }
        }
        
        // Average is sum / count
        average = sum / count;
        // Print Average
        System.out.printf("The average is %f", average);
    }
}