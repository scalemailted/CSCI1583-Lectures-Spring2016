//import scanner class
import java.util.Scanner;

//Create a class called Addition
public class Addition
{

    //Create a main method for java application
    public static void main(String[] args)
    {

        //Create a scanner and save it to a variable
        Scanner keyboard = new Scanner(System.in);
        
        //variable to hold first number
        int number1;
        
        //variable to hold a second number
        int number2;
        
        //variable to hold sum
        int sum;
        
        //prompt user for first number
        System.out.print("Enter first number: ");
        //get the first number
        number1 = keyboard.nextInt();
        
        //prompt user for the second number
        System.out.print("Enter second number: ");
        
         //get the second number
        number2 = keyboard.nextInt();
        
        //addition operation to find sum and save it
        sum =  number1 + number2;
        
        //report the result to the user
        System.out.printf("The sum of %d and %d is %d\n",number1,number2,sum);
    }
        
}