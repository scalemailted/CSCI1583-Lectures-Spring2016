public class Time
{
    //named constants
    private final int SECONDS_TO_MINUTE = 60;
    private final int MINUTES_TO_HOUR = 60;
    private final int SECONDS_TO_HOUR = SECONDS_TO_MINUTE * MINUTES_TO_HOUR;
    
    //instance variables
    private int seconds;
    
    public Time(int hour, int minute, int second)
    {
        setTime(hour, minute, second);
    }
    
    public Time(int hour, int minute)
    {
        this(hour, minute, 0);
    }
    
    public Time(int hour)
    {
        this(hour, 0, 0);
    }
    
    public Time()
    {
        this(0, 0, 0);
    }
    
    public Time(Time time)
    {
        this(time.getHour(), time.getMinute(), time.getSecond());
    }
    
    //setter for time
    public void setTime(int hour, int minute, int second)
    {
        if (hour < 0 || hour >= 24)
        {
            throw new IllegalArgumentException("hour must be 0-23");
        }
        if (minute < 0 || minute >= 60)
        {
            throw new IllegalArgumentException("minute must be 0-59");
        }
        if (second < 0 || second >= 60 )
        {
            throw new IllegalArgumentException("second must be 0-59");
        }
        this.seconds = hour*SECONDS_TO_HOUR + minute*SECONDS_TO_MINUTE + second;
    }
    
    public int getHour()
    {
        return this.seconds/SECONDS_TO_HOUR;
    }
    
    public int getMinute()
    {
        return this.seconds/SECONDS_TO_MINUTE - (this.getHour()*MINUTES_TO_HOUR);
    }
    
    public int getSecond()
    {
        return this.seconds % SECONDS_TO_MINUTE;
    }
    
    //universal time
    public String getUniversal()
    {
        return String.format("%02d:%02d:%02d",this.getHour(), this.getMinute(),this.getSecond());
    }
    
    //to string method
    public String toString()
    {
        return String.format("%d:%02d:%02d %s", (this.getHour()==0 || this.getHour()==12) ? 12 : this.getHour()%12, 
                                                this.getMinute(), this.getSecond(), 
                                                (this.getHour() < 12) ? "AM" : "PM");
    }
}