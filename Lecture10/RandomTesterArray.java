import java.util.Random;

public class RandomTesterArray
{
    public static void main(String[] args)
    {
        int[] frequency = new int[13];
        
        for(int i =0; i< 1000000; i++)
        {
            int diceSum = rollDice();
            frequency[diceSum]++;
        }//for loop
        for (int i=2; i<frequency.length; i++)
        {
            System.out.printf("%d: %d\n",i,frequency[i]);
        }
    }
    
    public static int rollDice(){
        Random  randomGenerator = new Random();
        int dice1 = randomGenerator.nextInt(6)+ 1;
        int dice2 = randomGenerator.nextInt(6) + 1;
        int sum = dice1 + dice2;
        return sum;
    }
}