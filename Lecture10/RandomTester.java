import java.util.Random;

public class RandomTester
{
    public static void main(String[] args)
    {
        int freq2 = 0;
        int freq3 = 0;
        int freq4 = 0;
        int freq5 = 0;
        int freq6 = 0;
        int freq7 = 0;
        int freq8 = 0;
        int freq9 = 0;
        int freq10 = 0;
        int freq11 = 0;
        int freq12 = 0;
        
        
        
        
        for(int i =0; i< 1000000; i++)
        {
            int diceSum = rollDice();
            switch(diceSum)
            {
                case 2: freq2++; break;
                case 3: freq3++; break;
                case 4: freq4++; break;
                case 5: freq5++; break;
                case 6: freq6++; break;
                case 7: freq7++; break;
                case 8: freq8++; break;
                case 9: freq9++; break;
                case 10: freq10++; break;
                case 11: freq11++; break;
                case 12: freq12++; break;
            }//end switch
        }//for loop
        System.out.printf("2:%d\n3:%d\n4:%d\n5:%d\n6:%d\n7:%d\n8:%d\n9:%d\n10:%d\n11:%d\n12:%d\n",freq2,freq3,freq4, freq5,freq6,freq7,freq8,freq9,freq10,freq11,freq12);
    }
    
    public static int rollDice(){
        Random  randomGenerator = new Random();
        int dice1 = randomGenerator.nextInt(6)+ 1;
        int dice2 = randomGenerator.nextInt(6) + 1;
        int sum = dice1 + dice2;
        return sum;
    }
}