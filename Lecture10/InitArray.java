public class InitArray
{
    public static void main(String[] args)
    {
        //initializing an array;
        //int[] myArray = new int[12];
        int[] myArray = {12, 23 ,43, 67, 88};
        
        for (int i=0; i< myArray.length; i++)
        {
            //myArray[i] = i;
            System.out.printf("%s%8s\n",i,myArray[i]);
        }
    }
}