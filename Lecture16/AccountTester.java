public class AccountTester
{
	public static void main(String[] args)
	{
		Account account1 = new Account("Jane Green", 100.00);
		Account account2 = new Account("Tom Blue", -100.00);

		System.out.println(account1);
		System.out.println(account2);

		account1.withdraw(50.00);
		account2.withdraw(100.00);

		System.out.println(account1);
		System.out.println(account2);

		account1.deposit(-50.00);
		account2.deposit(10000000.00);

		System.out.println(account1);
		System.out.println(account2);


	} 
}