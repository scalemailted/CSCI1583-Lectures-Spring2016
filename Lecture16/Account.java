public class Account
{
    private String name;
    private double balance;

    public Account(String name, double balance)
    {
    	this.name = name;
    	if (balance >= 0.0)
    	{
    		this.balance = balance;
    	}
    }

    public void withdraw(double amount)
    {
    	if (amount > 0.0  && this.balance >= amount)
    	{
    		this.balance -= amount;
    	}
    	else
    	{
    		System.out.println("You don't have enough money");
    	}
    }

    public void deposit(double amount)
    {
    	if (amount >= 0.0)
    	{
    		this.balance += amount;
    	}
    }

    public String toString()
    {
    	return String.format("name:%s, balance:$%.2f", this.name, this.balance);
    }



}
