import java.util.Scanner;
import java.util.InputMismatchException;

public class InputMismatch
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        
        boolean loop = true;
        while (loop)
        {
            try 
            {
                System.out.print("Enter the choice: ");
                int choice = input.nextInt();
                loop = false;
             }
            catch (InputMismatchException e) 
            {
                System.out.print("Enter number for choice.\n");
                input.nextLine();
            }
        }
    }
}