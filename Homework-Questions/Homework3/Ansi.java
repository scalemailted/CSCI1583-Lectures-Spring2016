public class Ansi
{
    private final static String ANSI_BLACK  = "\u001b[40m"; //Black Background
    private final static String ANSI_RED    = "\u001b[41m"; //Red Background
    private final static String ANSI_GREEN  = "\u001b[42m"; //Green Background 
    private final static String ANSI_YELLOW = "\u001b[43m"; //Yellow Background
    private final static String ANSI_BLUE   = "\u001b[44m"; //Blue Background
    private final static String ANSI_PURPLE = "\u001b[45m"; //Purple Background
    private final static String ANSI_CYAN   = "\u001b[46m"; //Cyan Background
    private final static String ANSI_WHITE  = "\u001b[47m"; //White Background
   
    private final static String ANSI_RESET = "\u001B[0m";  //Reset All Formatting
    
    
    public static void main(String[] args)
    {
        
        String[] colors = { ANSI_BLACK,
                            ANSI_RED,
                            ANSI_GREEN,
                            ANSI_YELLOW,
                            ANSI_BLUE,
                            ANSI_PURPLE,
                            ANSI_CYAN,
                            ANSI_WHITE, 
                            ANSI_RESET
                          };
                            
        for (String ansi : colors)
        {
            System.out.println(ansi + "Hello World" );
        }
        
        System.out.println("\u31B1");
        System.out.println("ㆱ");
    }
    
    
}