public class Date
{
    //instance variable
    private int day;
    private int month;
    private int year;
    
    //constructor
    public Date(int day, int month, int year)
    {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    
    //getter: day
    public int getDay()
    {
        return this.day;
    }
    
    //getter: month
    public int getMonth()
    {
        return this.month;
    }
    
    //getter: year
    public int getYear()
    {
        return this.year;
    }
    
    //setter: day
    public void setDay(int day)
    {
        this.day = day;
    }
    
    //setter: month
    public void setMonth(int month)
    {
        this.month = month;
    }
    
    //setter: day
    public void setYear(int year)
    {
        this.year = year;
    }
    
    public String getDate()
    {
        return String.format("%02d/%02d/%4d",this.day,this.month,this.year);
    }
}