import java.util.Scanner;

public class AccountTest
{
    public static void main(String[] args)
    {
        //create a scanner object
        Scanner input = new Scanner(System.in);
        //create an account object
        Account account1 = new Account();
        Account account2 = new Account();
        //display the initial state of account
        System.out.printf("The initial name is: %s\n", account1.getName());
        //prompt for a name
        System.out.print("Enter the name:");
        String name = input.nextLine();
        //prompt for a second name
        System.out.print("Enter another name: ");
        String name2 = input.nextLine();
        
        //Set name into account
        account1.setName(name);
        account2.setName(name2);
        //display new state of account
        System.out.printf("account1: %s\n", account1.getName());
        System.out.printf("account2: %s\n", account2.getName());
    }
}