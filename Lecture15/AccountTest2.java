import java.util.Scanner;

public class AccountTest2
{
    public static void main(String[] args)
    {
        //create a scanner object
        Scanner input = new Scanner(System.in);
        //create an account object
        Date birthGeorge = new Date(4,7,1900);
        Date birthLisa = new Date(14,3,2191);
        Account account1 = new Account("George", birthGeorge);
        Account account2 = new Account("Lisa", birthLisa);
        System.out.println(account1.getAccount());
        System.out.println(account2.getAccount());
        
    }
}