public class Account
{
    //instance variables or attributes
    private String name;
    private Date birthDate;
    
    //constructor
    public Account(String name, Date birth)
    {
        this.name = name;
        this.birthDate = birth;
    }
    
    public Account()
    {
        
    }
    
    //getter 
    public String getName()
    {
        return this.name;
    }
    
    //setter
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getAccount()
    {
        String name = this.name;
        String dob = this.birthDate.getDate();
        return String.format("Name:%s\nDOB:%s",name,dob);
    }
}