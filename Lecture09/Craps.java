import java.util.Random;

public class Craps
{
    //declare new datatype for our game's state
    private enum Status {CONTINUE, LOST, WON};
    
    //Important dice values
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int YO = 11;
    private static final int BOXCAR = 12;
    
    public static void main(String[] args)
    {
        //declare game status variable
        Status gameStatus;
        //declare my point variable
        int myPoint = 0;
        
        //get sum of rolling two dice
        int diceSum = rollDice();
        
        //check to see if that sum is win, lose, or a continue
        if (diceSum == SEVEN || diceSum == YO)
        {
            gameStatus = Status.WON;
        }
        else if (diceSum == SNAKE_EYES 
                || diceSum == TREY 
                || diceSum == BOXCAR)
        {
            gameStatus = Status.LOST;
        }
        else
        {
            //if we didn't win or lose then set our point
            myPoint = diceSum;
            gameStatus = Status.CONTINUE;
            System.out.printf("Your point is %d\n", myPoint);
        }
        
        
        //while we haven't won or lost we must continue to roll
        while (gameStatus == Status.CONTINUE)
        {
            //roll a new sum of dice
            diceSum = rollDice();
            //if new sum is seven then we lose
            if (diceSum == SEVEN)
            {
                gameStatus = Status.LOST;
            }
            //if new sum is our point we win
            else if (diceSum == myPoint)
            {
                gameStatus = Status.WON;
            }
        }
        
        //Report win or lost
        if (gameStatus == Status.WON)
        {
            System.out.println("Player won!");
        }
        else
        {
            System.out.println("Player lost!");
        }
        
    }
    
    //method that rolls two dice and returns the sum
    public static int rollDice()
    {
        Random randomGenrator = new Random();
        int dice1 = randomGenrator.nextInt(6)+1;
        int dice2 = randomGenrator.nextInt(6)+1;
        int diceSum = dice1 + dice2;
        System.out.printf("%d+%d=%d\n",dice1, dice2, diceSum);
        return diceSum;
    }
}