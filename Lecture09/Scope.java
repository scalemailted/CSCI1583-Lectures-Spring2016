public class Scope
{
    private static int x = 0;
    
    public static void main(String[] args)
    {
        int x = 6;
        System.out.printf("x is %d\n", x);
        staticVar(Scope.x);
        System.out.printf("x is %d\n", Scope.x);
    }
    
    public static void staticVar(int y)
    {
        x += 5;
    }
}