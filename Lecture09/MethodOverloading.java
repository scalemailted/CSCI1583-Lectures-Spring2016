public class MethodOverloading
{
    public static void main(String[] args)
    {
        int x = divide(3,4);
        System.out.printf("The result is %d\n", x);
        double y = divide(3.0,4.0);
        System.out.printf("The result is %.2f\n", y);
    }
    
    public static int divide(int x, int y)
    {
        int result = x/y;
        return result;
    }
    
    public static int divide(int x)
    {
        int result = x/2;
        return x;
    }
    
    public static double divide(double x, double y)
    {
        double result = x/y;
        return result;
    }
}