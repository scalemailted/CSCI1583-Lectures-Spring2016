public class MethodTest
{
    public static void main(String[] args)
    {
        System.out.println("Before method call");
       helloWorld();
       helloWorld();
       helloWorld();
       System.out.println("After method call");
       
    }
    
    public static void helloWorld()
    {
        System.out.println("Hello World");
        goodbyeWorld();
        return;
    }
    
    public static void goodbyeWorld()
    {
        System.out.println("Bye World");
        return;
    }
}