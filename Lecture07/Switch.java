import java.util.Scanner;

public class Switch
{
    public static void main(String[] args)
    {
        /*Data*/
        //setup a scanner
        Scanner input = new Scanner(System.in);
        
        String choice;      //arthiemtic operator
        double number1;     //lefthand operand
        double number2;     //righthand operand
        double result;      //result of operation
        
        /*processing*/

        //prompt for arthiemtic expression
        System.out.print("Enter an arithemtic expression (use spaces between operator)\n");
        
        //Parse user input
        number1 = input.nextDouble();       //lefthand operand
        choice = input.next();              //arithmetic operation
        number2 = input.nextDouble();       //righthand operand
        
        //multi-selection statement, input is user shoice 
        switch(choice)
        {
            case "+": result = number1 + number2;
                    System.out.printf("%.2f %s %.2f = %.2f\n", number1, choice, number2, result);
                    break;
                    
            case "-": result = number1 - number2;
                    System.out.printf("%.2f %s %.2f = %.2f\n", number1, choice, number2, result);
                    break;
                    
            case "*": result = number1 * number2;
                    System.out.printf("%.2f %s %.2f = %.2f\n", number1, choice, number2, result);
                    break;
                    
            case "/": result = number1 / number2;
                    System.out.printf("%.2f %s %.2f = %.2f\n", number1, choice, number2, result);
                    break;
                    
            default: System.out.println("That is not an operation");
        }
        
    }
}