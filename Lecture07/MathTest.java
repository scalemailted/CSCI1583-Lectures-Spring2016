public class MathTest
{
    public static void main(String[] args)
    {
        double x = 100.6;
        double y = 3;
        double result;
        
        result = Math.abs(x);
        System.out.printf("Math.abs(%.1f) = %f\n", x, result);
        
        result = Math.ceil(x);
        System.out.printf("Math.ceil(%.1f) = %f\n", x, result);
        
        result = Math.cos(x);
        System.out.printf("Math.cos(%.1f) = %f\n", x, result);
        
        result = Math.exp(x);
        System.out.printf("Math.exp(%.1f) = %f\n", x, result);
        
        result = Math.floor(x);
        System.out.printf("Math.floor(%.1f) = %f\n", x, result);
        
        result = Math.log(x);
        System.out.printf("Math.log(%.1f) = %f\n", x, result);
        
        result = Math.max(x,y);
        System.out.printf("Math.max(%.1f,%.0f) = %f\n", x, y, result);
        
        result = Math.min(x,y);
        System.out.printf("Math.min(%.1f,%.0f) = %f\n", x, y, result);
        
        result = Math.pow(x,y);
        System.out.printf("Math.pow(%.1f, %.0f) = %f\n", x, y, result);
        
        result = Math.sin(x);
        System.out.printf("Math.sin(%.1f) = %f\n", x, result);
        
        result = Math.sqrt(x);
        System.out.printf("Math.sqtr(%.1f) = %f\n", x, result);
        
        result = Math.tan(x);
        System.out.printf("Math.tan(%.1f) = %f\n", x, result);
    }
}