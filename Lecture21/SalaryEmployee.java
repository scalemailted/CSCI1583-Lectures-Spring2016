public class SalaryEmployee extends Employee
{
    private double monthlySalary;
    
    public SalaryEmployee(String firstName, String lastName, 
                          String ssn, Date dob, Date hireDate, 
                          double monthlySalary)
    {
        super(firstName, lastName, ssn, dob, hireDate);
        this.monthlySalary = monthlySalary;
    }
    
    public void setMonthlySalary(double salary)
    {
        this.monthlySalary = salary;
    }
    
    public double getMonthlySalary()
    {
        return this.monthlySalary;
    }
    
    public double getEarnings()
    {
        return this.getMonthlySalary()/2;
    }
    
    public String toString()
    {
        return String.format("%sSalaryEmployee:\nMonthly Salary: $%.2f\n",
                            super.toString(),this.getMonthlySalary() );
    }
}