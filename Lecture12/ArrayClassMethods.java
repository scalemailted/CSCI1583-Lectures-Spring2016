import java.util.Arrays;

public class ArrayClassMethods
{
    public static void main(String[] args)
    {
        int[] array = {2,4,3,5,1};
        
        //Sort array
        Arrays.sort(array);
        System.out.println("Sorted Array");
        printArray(array);
        
        //Fill array
        int[] fillArray = new int[10];
        Arrays.fill(fillArray, 7);
        System.out.println("Fill Array");
        printArray(fillArray);
        
        //copy arrays
        int[] arrayCopy = new int[array.length];
        System.arraycopy(array, 0, arrayCopy, 0, array.length);
        System.out.println("Copy array");
        printArray(arrayCopy);
        
        //Equality
        boolean b = Arrays.equals(array, fillArray);
        System.out.printf("array1 == array2: %b\n",b);
        
        //search
         int location = Arrays.binarySearch(array, 3);
         System.out.printf("The location of 3 in arrays is index %d", location);
        
    }
    
    public static void printArray(int[] arr)
    {
        for (int i : arr)
        {
            System.out.println(i);
        }
    }
    
}