public class CommandLineArgs
{
    public static void main(String[] x)
    {
        if (x.length < 2)
        {
            System.out.println("You must pass 2 arguments");
        }
        else
        {
            System.out.println(x[0]);
            System.out.println(x[1]);
        }
    }
}