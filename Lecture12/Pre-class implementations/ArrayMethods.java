import java.util.Arrays;

public class ArrayMethods
{
    public static void main(String[] args)
    {
        int[] array = {2,4,3,5,1};
        
        //Sort array
        Arrays.sort(array); 
        System.out.println("Array");
        printArray(array);
        
        //Fill array
        int[] arrayFill = new int[5];
        Arrays.fill(arrayFill, 7);
        System.out.println("Filled Array");
        printArray(arrayFill);
        
        
        //copy array
        int[] arraycopy = new int[array.length];
        System.arraycopy(array, 0, arraycopy, 0, array.length);
        System.out.println("Array copy");
        printArray(arraycopy);
        
        //equality
        boolean b = Arrays.equals(array, arraycopy);
        System.out.printf("array == arraycopy is %b\n", b );
        
        //search
        int location = Arrays.binarySearch(array, 3);
        System.out.printf("The index of 3 in the array is %d\n", location);
        
    }
    
    public static void printArray(int[] arr)
    {
        for (int i: arr)
        {
            System.out.println(i);
        }
    }
}