import java.util.Scanner;
//java CommandLineArgs 2 2 X 0 X 0


public class RedirectingData
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        
        int width = input.nextInt(); 
        int height = input.nextInt();
        
        for(int j=0; j<height; j++)
        {
            for(int i=0; i<width; i++)
            {
                String tile = input.next() + " ";
                System.out.print(tile);
            }
            System.out.println();
        }
    }
}