import java.util.*;

public class ArrayListInit
{
    public static void main(String[] args)
    {
        ArrayList<String> places = new ArrayList<String>( Arrays.asList("Buenos Aires", "Córdoba", "La Plata"));
        System.out.println(places.toString());
    }
}