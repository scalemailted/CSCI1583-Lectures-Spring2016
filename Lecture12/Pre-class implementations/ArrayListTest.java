import java.util.ArrayList;

public class ArrayListTest
{
    public static void main(String[] args)
    {
        ArrayList<String> items = new ArrayList<String>(); //no size
        items.add("red");       //add to list
        items.add(0,"yellow");  //insert at front of list
        
        //Print array 
        System.out.println(items.toString());
        
        //Print element from array
        System.out.println(items.get(0));
        
        //method call
        display(items);
        
        //Remove element from array
        items.remove("yellow");
        System.out.println(items);
        
        //Check if list has an item
        boolean b = items.contains("red");
        System.out.printf("The list contains red? %b\n", b);
        
        //Check size of list
        System.out.printf("The size of list is: %d\n", items.size());
        
        
        
    }
    
    public static void display(ArrayList<String> items)
    {
        System.out.println("The items are:");
        for (String s : items)
        {
            System.out.println(s);
        }
    }
    
}