public class CommandLineArgs
{
    public static void main(String[] args)
    {
        if (args.length < 2)
        {
            System.out.println("You must enter 2 command line arguments");
        }
        else
        {
            System.out.println(args[0]);
            System.out.println(args[1]);
        }
    }
}