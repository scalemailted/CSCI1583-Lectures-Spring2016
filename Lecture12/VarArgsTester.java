public class VarArgsTester
{
    public static void main(String[] args)
    {
        double d1 = 0.0;
        double d2 = 1.0;
        double d3 = 2.0;
        double d4 = 3.0;
        
        System.out.printf("d1:%.1f, d2:%.1f, d3:%.1f, d4:%.1f\n",d1,d2,d3,d4);
        System.out.printf("Average of d1,d2 = %.2f\n", average(d1,d2));
        System.out.printf("Average of d1,d2,d3 = %.2f\n", average(d1,d2,d3));
        System.out.printf("Average of d1,d2,d3,d4 = %.2f\n", average(d1,d2,d3,d4));
        
    }
    
    public static double average(double... numbers)
    {
        double total = 0;
        for (double element : numbers)
        {
            total += element;
        }
        return total / numbers.length;
    }
}