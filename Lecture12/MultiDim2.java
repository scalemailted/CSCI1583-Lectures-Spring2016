public class MultiDim2
{
    public static void main(String[] args)
    {
        int[][] myArray = new int[5][];
        myArray[0] = new int[5];
        myArray[1] = new int[2];
        
        //dereference
        myArray[2][1] = 5;
    }
}