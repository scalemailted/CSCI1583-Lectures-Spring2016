import java.util.ArrayList;

public class ArrayListTest
{
    public static void main(String[] args)
    {
        ArrayList<String> items = new ArrayList<String>(); //no size
        items.add("red");
        System.out.println(items);
        items.add(0, "yellow");
        System.out.println(items);
        
        //print element from array
        System.out.println(items.get(0));
        
        display(items);
        
        System.out.println(items);
        items.remove("yellow");
        System.out.println(items);
        
        //print element from array
        System.out.println(items.get(0));
        
        boolean b = items.contains("yellow");
        System.out.println(b);
        
        System.out.println(items.size());
        
        
        
    }
    
    public static void display(ArrayList<String> items)
    {
        System.out.println("items: ");
        for (String s : items)
        {
            System.out.println(s);
        }
    }
}