import java.util.Scanner;

public class MaxValue2
{
    private static double number1;
    private static double number2;
    private static double number3;
    
    public static void main(String[] args)
    {
        MaxValue2.getValuesFromUser();
        double result = MaxValue2.maxValue(number1,number2,number3);
        System.out.printf("The max value is %f\n", result);
    }
    
    public static void getValuesFromUser()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter 3 numbers seperated with spaces:");
        number1 = input.nextDouble();
        number2 = input.nextDouble();
        number3 = input.nextDouble();
    }
    
    public static double maxValue(double x, double y, double z)
    {
        double max = x;
        if (y > max)
        {
            max = y;
        }
        if (z > max)
        {
            max = z;
        }
        return max;
    }
}