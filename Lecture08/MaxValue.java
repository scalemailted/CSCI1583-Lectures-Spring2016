import java.util.Scanner;

public class MaxValue
{
    private static int number1;
    private static int number2;
    private static int number3;
    
    public static void main(String[] args)
    {
        MaxValue.getValuesFromUser();
        int result = MaxValue.maxValue(number1,number2,number3);
        System.out.printf("The max value is %d\n", result);
    }
    
    public static void getValuesFromUser()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter 3 numbers seperated with spaces:");
        number1 = input.nextInt();
        number2 = input.nextInt();
        number3 = input.nextInt();
    }
    
    public static int maxValue(int x, int y, int z)
    {
        int max = x;
        if (y > max)
        {
            max = y;
        }
        if (z > max)
        {
            max = z;
        }
        return max;
    }
}