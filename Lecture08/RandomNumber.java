import java.util.Random;

public class RandomNumber
{
    public static void main(String[] args)
    {
        Random randomGenerator = new Random();
        randomGenerator.setSeed(0);
        for (int i=0; i<100; i++)
        {
            
            int randomNumber = randomGenerator.nextInt(6)+1;
            System.out.printf("%d ", randomNumber);
        }
    }
}