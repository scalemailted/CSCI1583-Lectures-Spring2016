

public class Hello
{
    public static void main(String[] args)
    {
        //variable declaration
        int number1 = 1;
        int number2 = number1;
        double number3 = 0.003;
        char character = 'a';
        boolean yes = true;
        
        System.out.println(yes);
        
        //output operation
        System.out.printf("The value of variables: %f \n", number3);
        
    }
    
}