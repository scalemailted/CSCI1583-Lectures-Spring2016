import java.util.Scanner;

public class Input
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter your name: ");
        String name = input.next();   
        System.out.printf("Hello %s\n",name);
    }
}