public class Interest
{
    public static void main(String[] args)
    {
        /*data setup*/
        double rate = 0.05;      // rate of interest
        double principal = 1000; // the original amount
        double amount;           // the final amount
        
        //header for output
        System.out.printf("%s%20s","Year","Amount on deposit\n");
        
        /*process the data and get solution*/
        // repetition from 1st year to 10th year
        for (int year = 1; year <= 10; year++)
        {
            //amount = principal * (1+rate)**year
            amount = principal * Math.pow(1.0+rate, year);
            //print amount
            System.out.printf("%4d%20.2f\n", year, amount);
        }
    
    }
}