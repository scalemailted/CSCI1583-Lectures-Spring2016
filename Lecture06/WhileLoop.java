public class WhileLoop
{
    public static void main(String[] args)
    {
        //while loop
        int counter = 1;
        while (counter <= 10)
        {
            System.out.printf("%d ", counter);
            counter++;
        }
        System.out.print("\n");
        
        //for loop
        for(int i=1; i<=10; i++)
        {
            System.out.printf("%d ", i);
        }
        System.out.print("\n");
        
        //for loop
        int var3 = 0;
        for(;var3<=10; )
        {
            System.out.printf("%d ", var3);
            var3++;
        }
        System.out.print("\n");
    }
}