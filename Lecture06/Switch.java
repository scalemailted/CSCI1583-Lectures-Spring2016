import java.util.Scanner;

public class Switch
{
    public static void main(String[] args)
    {
        /*Data*/
        //setup a scanner
        Scanner input = new Scanner(System.in);
        //operation choice
        int choice;
        //first number
        int number1;
        //second number
        int number2;
        //result
        int result;
        
        /*processing*/
        //prompt for the operation
        System.out.println("1)Add\n2)Subtract\n3)Multiply\n4)Divide");
        //get operation choice
        choice = input.nextInt();
        
        //prompt for first number
        System.out.print("Enter the first number: ");
        //get first number
        number1 = input.nextInt();
        
        //prompt for first number
        System.out.print("Enter the second number: ");
        //get first number
        number2 = input.nextInt();
        
        //based on operation perform the calculation
        switch(choice)
        {
            case 1: result = number1 + number2; 
                    break;
            case 2: result = number1 - number2;
                    break;
            case 3: result = number1 * number2;
                    break;
            case 4: result = number1 / number2;
                    break;
            default: result = 0;
        }
        System.out.printf("The result is: %d\n", result);
        
        
        
    }
}