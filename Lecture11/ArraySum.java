public class ArraySum
{
    public static void main(String[] args)
    {
        int[] array = {23, 43, 34, 56, 222 };
        int sum = 0;
        for (int element : array)
        {
            sum += element;
        }
        System.out.printf("The sum is: %d", sum);
    }
    
}