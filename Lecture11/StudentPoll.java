//Poll analysis program for ranking food 
public class StudentPoll
{
    public static void main(String[] args)
    {
        /**data**/
        //data for student responses
        int[] responses = {1,2,3,4,5,1,2,5,3,1,1,2,4,3,3,2,1,3,45};
        
        //frequency for each score
        int[] frequency = new int[6];
        
        /**processing**/
        //for each answer in student scores
         //we want to update the frequency for that score
        for (int score : responses)
        {
            try 
            {
                frequency[score]++;
            } 
            catch(ArrayIndexOutOfBoundsException e) 
            {
                System.out.println(e);
            }
            
        }
        
        for (int i=1; i<frequency.length; i++)
        {
            System.out.printf("%d: %d\n",i,frequency[i]);
        }
       
        
    }
}