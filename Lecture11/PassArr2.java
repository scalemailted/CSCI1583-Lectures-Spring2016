public class PassArr2
{
    public static void main(String[] args)
    {
        int[] arr = {1,2,3,4,5};
        
        for (int i: arr)
        {
            System.out.printf("%d\n",i);
            
        }
        
         modify(arr);
        
        for (int i: arr)
        {
            System.out.printf("%d\n",i);
            
        }
        
    }
    
    public static void modify(int[] a)
    {
        for (int i=0; i< a.length;i++)
        {
            a[i] *= 5;
        }
    }
}