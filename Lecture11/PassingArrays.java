public class PassingArrays
{
    public static void main(String[] args)
    {
        int[] array = {1,2,3,4,5};
        displayArray(array);
        //modifyArray(array);
        displayArray(array);
        displayElement(array[3]);
        
        
        
    }
    
    public static void displayArray(int[] arr)
    {
        for (int item : arr)
        {
            System.out.printf("%d, ",item);
        }
        System.out.println();
        return;
    }
    
    public static void modifyArray(int[] arr)
    {
        for (int i=0; i<arr.length;i++)
        {
            arr[i] = 0;
        }
        return;
    }
    
    public static void displayElement(int element)
    {
        System.out.printf("%d\n",element);
        return;
    }
}