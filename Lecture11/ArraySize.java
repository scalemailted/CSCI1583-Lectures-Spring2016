public class ArraySize
{
    public static void main(String[] args)
    {
        int size = 10;
        int[] array = {1,4,78,23,43,54,56};
        for (int element : array)
        {
            System.out.printf("%d\n",element);
        }
    }
}