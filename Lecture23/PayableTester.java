public class PayableTester
{
    public static void main(String[] args)
    {
        Date hire = new Date(1,1,2000);
         
         Payable[] costlyThings = new Payable[5];
         costlyThings[0] =  new SalaryCommissionEmployee("Jim", "Frankie", "123-45-6789", new Date(11,10,1991), hire, 1000, 0.25,300);
         costlyThings[1] =  new CommissionEmployee("Gary", "Hen", "123457387", new Date(11,10,1972), hire, 100000, 0.50);
         costlyThings[2] =  new SalaryEmployee("Kelly", "Lowe", "3268762834", new Date(1,1,2100), hire, 3000000);
         costlyThings[3] =  new Invoice("A", 100.00);
         costlyThings[4] =  new Invoice("Z", 20.00);
         for (Payable e : costlyThings)
         {
             System.out.println(e.cost());
         }
         
    }
}