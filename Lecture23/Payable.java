public interface Payable
{
    public abstract double cost();
}