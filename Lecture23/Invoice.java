public class Invoice implements Payable
{
    double totalCost;
    String partNumber;
    
    public Invoice(String partNumber, double totalCost)
    {
        this.partNumber = partNumber;
        this.totalCost = totalCost;
    }
    
    public double cost()
    {
        return this.totalCost;
    }

    
}