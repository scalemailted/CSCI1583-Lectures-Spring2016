public enum Item
{
    KEY("This is a silver key", "key"),
    SWORD("This is a sharp sword", "sword"),
    SHIELD("This is a wooden shield", "armor");
    
    private final String description;
    private final String itemType;
    
    Item(String description, String itemType)
    {
        this.description = description;
        this.itemType = itemType;
    }
    
    public String getDescription()
    {
        return this.description;
    }
    
    public String getItemType()
    {
        return this.itemType;
    }
}