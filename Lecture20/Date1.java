public class Date1
{
    //instance variable
    private int day;
    private int month;
    private int year;
    
    private static final int[] DAYS_PER_MONTH = 
        {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    
    //constructor
    public Date1(int day, int month, int year)
    {
        this.setDate(day, month, year);
        
    }
    
    //getter: day
    public int getDay()
    {
        return this.day;
    }
    
    //getter: month
    public int getMonth()
    {
        return this.month;
    }
    
    //getter: year
    public int getYear()
    {
        return this.year;
    }
    
    //setter: day
    private void setDay(int day)
    {
        int month = this.getMonth();
        int year = this.getYear();
        if (day <= 0 || (day > DAYS_PER_MONTH[month] && (month != 2 && day != 29) ) )
        {
            throw new IllegalArgumentException("incorrect day");
        }
        if ( (day == 29 && month == 2) && !( (year % 4 == 0 && year %100 != 0) || year % 400 == 0) )
        {
            throw new IllegalArgumentException("incorrect day");
        }
        
        this.day = day;
    }
    
    //setter: month
    private void setMonth(int month)
    {
        if (month <= 0 || month > 12)
        {
            throw new IllegalArgumentException("month must be between 1-12");
        }
        this.month = month;
    }
    
    //setter: day
    private void setYear(int year)
    {
        if (year < 1900)
        {
            throw new IllegalArgumentException("year must be after 1900");
        }
        this.year = year;
    }
    
    public void setDate(int day, int month, int year)
    {
        this.setYear(year);
        this.setMonth(month);
        this.setDay(day);
    }
    
    public String toString()
    {
        return String.format("%02d/%02d/%4d",this.day,this.month,this.year);
    }
}