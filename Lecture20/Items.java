public enum Items
{
    KEY("This is a silver key", "trinket", 0, 0),
    SWORD("This is a silver sword", "weapon", 10, 2),
    SHIELD("This is a silver shield", "armor", 2, 10);
    
    private String description;
    private String type;
    private int attack;
    private int defense;
    
    Items(String description, String type, int attack, int defense)
    {
        this.description = description;
        this.type = type;
        this.attack = attack;
        this.defense = defense;
    }
    
    public String getDescription()
    {
        return this.description;
    }
}