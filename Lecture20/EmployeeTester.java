public class EmployeeTester
{
    public static void main(String[] args)
    {
        System.out.printf("count: %d\n",Employee.getCount());
        Date hire = new Date(1,1,2000);
        Employee e1 = new Employee("Bob", "Singer", "111-11-1111", new Date(1,1,1901), hire);
        System.out.printf("count: %d\n",e1.getCount());
        System.out.println(e1);
        
         Employee e2 = new Employee("Jane", "Singer", "111-11-1111", new Date(1,1,1901), hire);
         System.out.printf("count: %d\n",e2.getCount());
    }
}