public class TimeTester1
{
    public static void main(String[] args)
    {
        
        Time1 t1 = new Time1(0);
        System.out.println(t1);
        System.out.println(t1.getUniversal());
        
        Time1 t2 = new Time1(60*60*12+ 30*60 + 12);
        System.out.println(t2);
        System.out.println(t2.getUniversal());
        
        try 
        {
            t1.setTime(86400);
        }
        catch(IllegalArgumentException e) 
        {
            System.out.printf("Exception: %s\n",e.getMessage());
            t1.setTime(86399);
        }
        System.out.println(t1.getUniversal());
        System.out.println(t1);
    
        
    }
}