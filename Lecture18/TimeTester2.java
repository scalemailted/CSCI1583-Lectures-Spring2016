public class TimeTester2
{
    public static void main(String[] args)
    {
        Time2 t1 = new Time2(1,34);
        System.out.println(t1);
        System.out.println(t1.getUniversal());
        try 
        {
            t1.setTime(111,111,1);
        }
        catch(IllegalArgumentException e) 
        {
            System.out.printf("Exception: %s\n",e.getMessage());
            t1.setTime(16,16,16);
        }
        System.out.println(t1.getUniversal());
        System.out.println(t1);
        
    }
}