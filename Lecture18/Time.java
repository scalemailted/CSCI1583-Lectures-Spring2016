public class Time
{
    //instance variables
    private int hour;
    private int minute;
    private int second;
    
    public Time(int hour, int minute, int second)
    {
        setTime(hour, minute, second);
    }
    
    public Time(int hour, int minute)
    {
        this(hour, minute, 0);
    }
    
    public Time(int hour)
    {
        this(hour, 0, 0);
    }
    
    public Time()
    {
        this(0, 0, 0);
    }
    
    //setter for time
    public void setTime(int hour, int minute, int second)
    {
        if (hour < 0 || hour >= 24)
        {
            throw new IllegalArgumentException("hour must be 0-23");
        }
        if (minute < 0 || minute >= 60)
        {
            throw new IllegalArgumentException("minute must be 0-59");
        }
        if (second < 0 || second >= 60 )
        {
            throw new IllegalArgumentException("second must be 0-59");
        }
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    
    //universal time
    public String getUniversal()
    {
        return String.format("%02d:%02d:%02d",this.hour, this.minute,this.second);
    }
    
    //to string method
    public String toString()
    {
        return String.format("%d:%02d:%02d %s", (this.hour==0 || this.hour==12) ? 12 : hour%12, 
                                                this.minute, this.second, 
                                                (this.hour < 12) ? "AM" : "PM");
    }
}