public class TimeTester
{
    public static void main(String[] args)
    {
        Time t1 = new Time(1,34);
        System.out.println(t1);
        System.out.println(t1.getUniversal());
        try 
        {
            t1.setTime(111,111,1);
        }
        catch(IllegalArgumentException e) 
        {
            System.out.printf("Exception: %s\n",e.getMessage());
            t1.setTime(16,16,16);
        }
        System.out.println(t1.getUniversal());
        System.out.println(t1);
        
    }
}