public class Time1
{
    //Named constants for number of seconds in minutes & hours
    private final int SECONDS_IN_MINUTE = 60;
    private final int MINUTES_IN_HOUR   = 60;
    private final int SECONDS_IN_HOUR   = SECONDS_IN_MINUTE * MINUTES_IN_HOUR;
    
    
    //instance variables
    private int seconds;
    
    
    public Time1(int seconds)
    {
        if (seconds < 0 || seconds > 86399)
        {
            throw new IllegalArgumentException("seconds must be between 0-86399");
        }
        this.seconds = seconds;
    }
    
    //setter for time
    public void setTime(int seconds)
    {
        if (seconds < 0 || seconds > 23*SECONDS_IN_HOUR + 59*SECONDS_IN_MINUTE + 59)
        {
            throw new IllegalArgumentException("seconds must be between 0-86399");
        }
        this.seconds = seconds;
    }
    
    //universal time
    public String getUniversal()
    {
        int hour   = this.seconds/(SECONDS_IN_HOUR); 
        int minute = this.seconds/(SECONDS_IN_MINUTE) - (hour * MINUTES_IN_HOUR); 
        int second = this.seconds % SECONDS_IN_MINUTE;
        return String.format("%02d:%02d:%02d",hour, minute,second);
    }
    
    
    //to string method
    public String toString()
    {
        int hour   = this.seconds/(SECONDS_IN_HOUR); 
        int minute = this.seconds/(SECONDS_IN_MINUTE) - (hour * MINUTES_IN_HOUR); 
        int second = this.seconds % SECONDS_IN_MINUTE;
        return String.format("%d:%02d:%02d %s", (hour==0 || hour==12) ? 12 : hour%12, 
                                                minute, second, 
                                                (hour < 12) ? "AM" : "PM");
    }
}