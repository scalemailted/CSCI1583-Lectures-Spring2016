public class Time2
{
    //Named constants for number of seconds in minutes & hours
    private final int SECONDS_IN_MINUTE = 60;
    private final int MINUTES_IN_HOUR   = 60;
    private final int SECONDS_IN_HOUR   = SECONDS_IN_MINUTE * MINUTES_IN_HOUR;
    
    
    //instance variables
    private int seconds;
    
    
    public Time2()
    {
        
        this(0, 0, 0);
    }
    
    public Time2(int hours)
    {
        
        this(hours, 0, 0);
    }
    
    public Time2(int hours, int minutes)
    {
        
        this(hours, minutes, 0);
    }
    
    public Time2(int hours, int minutes, int seconds)
    {
        
        setTime(hours, minutes, seconds);
    }
    
    //setter for time
    public void setTime(int hours, int minutes, int seconds)
    {
        if (hours < 0 || hours >= 24)
        {
            throw new IllegalArgumentException("hours must be between 0-23");
        }
        if (minutes < 0 || minutes >= 60)
        {
            throw new IllegalArgumentException("minutes must be between 0-59");
        }
        if (seconds < 0 || seconds >= 60)
        {
            throw new IllegalArgumentException("seconds  must be between 0-59");
        }
        this.seconds = hours*SECONDS_IN_HOUR + minutes*SECONDS_IN_MINUTE + seconds;
    }
    
    //universal time
    public String getUniversal()
    {
        int hour   = this.seconds/(SECONDS_IN_HOUR); 
        int minute = this.seconds/(SECONDS_IN_MINUTE) - (hour * MINUTES_IN_HOUR); 
        int second = this.seconds % SECONDS_IN_MINUTE;
        return String.format("%02d:%02d:%02d",hour, minute,second);
    }
    
    
    //to string method
    public String toString()
    {
        int hour   = this.seconds/(SECONDS_IN_HOUR); 
        int minute = this.seconds/(SECONDS_IN_MINUTE) - (hour * MINUTES_IN_HOUR); 
        int second = this.seconds % SECONDS_IN_MINUTE;
        return String.format("%d:%02d:%02d %s", (hour==0 || hour==12) ? 12 : hour%12, 
                                                minute, second, 
                                                (hour < 12) ? "AM" : "PM");
    }
}