import java.util.Scanner;

public class Selection
{
    public static void main(String[] args)
    {
        /*Data Setup*/
        //setup a scanner object
        Scanner input = new Scanner(System.in);
        
        //setup a grade variable 
        int grade;
        
        /*Processing*/
        //prompt user for grade
        System.out.print("Enter grade: ");
        
        //get user input and save into variable
        grade = input.nextInt();
        
        //decide if user passed
        if ( grade >= 60) 
        {
            //Print you passed
            System.out.println("You passed!");
            System.out.println("Congrats");
        }
        else
        {
            //Print you failed
            System.out.println("You failed");
            System.out.println("Too bad!");
        }
        
    }
}