public class DanglingElse
{
    public static void main(String[] args)
    {
        int x = 6;
        int y = 5;
        if (x > 5)
        {
            if (y > 5)
                System.out.print("x > 5 and y > 5");
        }
        else 
            System.out.print("x < 5");
    }
}