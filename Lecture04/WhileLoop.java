public class WhileLoop
{
    public static void main(String[] args)
    {
        //create a variable and set to 3
        int product = 3; 
        //while product is under 100 multiply 3 to it
        while (product < 100)
        {
            product = product * 3;
            System.out.printf("Product is: %d\n", product);
        }
    }
}