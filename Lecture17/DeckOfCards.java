import java.util.Random;

public class DeckOfCards
{
    public final static int NUMBER_OF_CARDS = 52;
    private Card[] deck;
    private Random randomGenerator;
    private int topCard = 0; 
    
    //Constructor
    public DeckOfCards()
    {
        randomGenerator = new Random();
        
        String[] suits = {"Diamonds", "Spades", "Hearts", "Clubs"};
        String[] ranks = {"Ace", "Deuce", "Three", "Four", "Five", 
                          "Six", "Seven", "Eight", "Nine", "Ten",
                          "Jack", "Queen", "King"};
        
        this.deck = new Card[NUMBER_OF_CARDS];
        for(String suit : suits)
        {
            for (String rank : ranks)
            {
                this.deck[topCard++] = new Card(suit, rank); 
            }
        }
        this.topCard = 0;
        
    }
    
    //deal
    public Card deal()
    {
        if (this.topCard >= NUMBER_OF_CARDS)
        {
            this.topCard = 0;
        }
        return this.deck[this.topCard++];
    }
    
    //shuffle
    public void shuffle()
    {
        for(int currentCard=0; currentCard<NUMBER_OF_CARDS; currentCard++)
        {
            int randomCard = randomGenerator.nextInt(NUMBER_OF_CARDS);
            Card thisCard = this.deck[currentCard];
            this.deck[currentCard] = this.deck[randomCard];
            this.deck[randomCard] = thisCard;
            
        }
        this.topCard=0;
    }
    
    public String toString()
    {
        String cardString = "";
        for(Card card : this.deck)
        {
            cardString += String.format("%s\n", card);
        }
        return cardString;
    }
}