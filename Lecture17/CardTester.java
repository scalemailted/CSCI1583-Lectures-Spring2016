import java.util.Scanner;

public class CardTester
{
    public static void main(String[] args)
    {
        //Card testCard = new Card("Spades","Ace");
        //System.out.println(testCard);
        DeckOfCards deck = new DeckOfCards();
        
        //System.out.print("Draw next card?");
        //int choice = input.nextInt();
        for(int i=0; i<deck.NUMBER_OF_CARDS;i++)
        {
            System.out.printf("%s\n",deck.deal());
        }
        System.out.print("\n\n\n");
        deck.shuffle();
        for(int i=0; i<deck.NUMBER_OF_CARDS;i++)
        {
            System.out.printf("%s\n",deck.deal());
        }
    }
}