public class InstanceofTester
{
    public static void main(String[] args)
    {
        Animal[] animals = new Animal[3];
        animals[0] = new Frog();
        animals[1] = new Dog();
        animals[2] = new Bird();
        
        for (Animal pet : animals)
        {
            if (pet instanceof Dog)
            {
                System.out.printf("%s has four legs\n", pet);
                System.out.println( ((Dog)pet).bark() );
            }
            
            else if (pet instanceof Bird)
            {
                System.out.printf("%s has wings\n", pet);
            }
            
            else if (pet instanceof Frog)
            {
                System.out.printf("%s has webbed feet\n", pet);
            }
        }
        
    }
}


abstract class Animal
{
    boolean isAlive;
    
    public Animal()
    {
        this.isAlive = true;
    }
    
    @Override
    public String toString()
    {
        return "Animal";
    }
    
    public abstract String move();
    
}

class Frog extends Animal
{
    @Override
    public String toString()
    {
        return "Frog";
    }
    
    @Override
    public String move()
    {
        return "The frog hops";
    }
    
}

class Dog extends Animal
{
    @Override
    public String toString()
    {
        return "Dog";
    }
    
    @Override
    public String move()
    {
        return "The dog runs";
    }
    
    public String bark()
    {
        return "arf arf";
    }
}


class Bird extends Animal
{
    @Override
    public String toString()
    {
        return "Bird";
    }
    
    @Override
    public String move()
    {
        return "The bird flies";
    }
}