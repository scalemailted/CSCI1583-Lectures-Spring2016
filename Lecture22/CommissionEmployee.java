public class CommissionEmployee extends Employee
{
    //instance variables
    private double commissionRate;
    private double grossSales;
    
    
    //constructor
    public CommissionEmployee(String firstName, String lastName, 
                              String ssn, Date dob, Date hireDate, 
                              double grossSales, double commissionRate)
    {
        super(firstName, lastName, ssn, dob, hireDate);
        this.grossSales = grossSales;
        this.commissionRate = commissionRate;
    }
    
    public String toString()
    {
        return String.format("%s, %s\n%s\ndob:%s\nhire:%s\nCommision Employee:\nsales:$%.2f percentage:%.2f\n",
                              this.getLastName(),
                              this.getFirstName(),
                              this.getSSN(),
                              this.getDOB(),
                              this.getHireDate(),
                              this.getGrossSales(),
                              this.getCommissionRate());
    }
    
    
    public double getCommissionRate()
    {
        return this.commissionRate;
    }
    
    public double getGrossSales()
    {
        return this.grossSales;
    }
    
    public double getEarnings()
    {
        return this.getGrossSales() * this.getCommissionRate();
    }
}