public class SalaryCommissionEmployee extends CommissionEmployee
{
    private double monthlySalary;
    
    public SalaryCommissionEmployee(String firstName, String lastName, 
                                    String ssn, Date dob, Date hireDate, 
                                    double grossSales, double commissionRate,
                                    double monthlySalary)
    {
        super(firstName, lastName, ssn, dob, 
              hireDate, grossSales, commissionRate);
              
        this.monthlySalary = monthlySalary;
        
    }
    
    public double getMonthlySalary()
    {
        return this.monthlySalary;
    }
    
    public double getEarnings()
    {
        return super.getEarnings() + this.monthlySalary/2;
    }
    
    @Override
    public String toString()
    {
        return String.format("%s, %s\n%s\ndob:%s\nhire:%s\nCommision+Salary Employee:\nbase salary: %.2f sales:$%.2f percentage:%.2f\n",
                              this.getLastName(),
                              this.getFirstName(),
                              this.getSSN(),
                              this.getDOB(),
                              this.getHireDate(),
                              this.getMonthlySalary(),
                              this.getGrossSales(),
                              this.getCommissionRate());
    }
    
}