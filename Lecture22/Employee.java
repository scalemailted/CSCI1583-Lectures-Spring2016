public abstract class Employee
{
    private static int count = 0; 
    
    //instance variables
    String firstName;
    String lastName;
    String ssn;
    Date dob;
    Date hireDate;
    
    //constructor
    public Employee(String firstName, String lastName, String ssn, Date dob, Date hireDate)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.ssn = ssn;
        this.dob = dob;
        this.hireDate = hireDate;
        Employee.count++;
    }
    
    //Getters
    public String getFirstName()
    {
        return this.firstName;
    }
    
    public String getLastName()
    {
        return this.lastName;
    }
    
    public String getSSN()
    {
        return this.ssn;
    }
    
    public Date getDOB()
    {
        return this.dob;
    }
    
    public Date getHireDate()
    {
        return this.hireDate;
    }
    
    public String toString()
    {
        return String.format("%s, %s\n%s\ndob:%s\nhire:%s\n",
                              this.getLastName(),
                              this.getFirstName(),
                              this.getSSN(),
                              this.getDOB(),
                              this.getHireDate());
    }
    
    public static int getCount()
    {
        return Employee.count;
    }
    
    public abstract double getEarnings();
}