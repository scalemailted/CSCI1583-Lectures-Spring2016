public class EmployeeTester
{
    public static void main(String[] args)
    {
        Date hire = new Date(1,1,2000);
        /*
        System.out.printf("count: %d\n",Employee.getCount());
        Date hire = new Date(1,1,2000);
        Employee e1 = new Employee("Bob", "Singer", "111-11-1111", new Date(1,1,1901), hire);
        System.out.printf("count: %d\n",e1.getCount());
        System.out.println(e1);
        
         Employee e2 = new Employee("Jane", "Singer", "111-11-1111", new Date(1,1,1901), hire);
         System.out.printf("count: %d\n",e2.getCount());
         System.out.printf("The class type is: %s\n", e2.getClass());
         */
         //Date hire = new Date(1,1,2000);
         //CommissionEmployee e3 = new CommissionEmployee("Jane", "Singer", "111-11-1111", new Date(1,1,1901), hire, 20000, 0.25);
         //System.out.println(e3);
         //System.out.printf("The earnings is: $%.2f\n", e3.getEarnings());
         
         //SalaryEmployee e4 = new SalaryEmployee("Jim", "Frankie", "123-45-6789", new Date(11,10,1991), hire, 1000);
         //System.out.println(e4);
         //System.out.printf("The earnings is: $%.2f\n", e4.getEarnings());
         
         //Employee e5 = new SalaryEmployee("Jim", "Frankie", "123-45-6789", new Date(11,10,1991), hire, 1000);
         //System.out.println(e5.getClass());
         //SalaryEmployee se = (SalaryEmployee)(e5);
         //System.out.println(se.getClass());
         
         
         //SalaryCommissionEmployee e5 = new SalaryCommissionEmployee("Jim", "Frankie", "123-45-6789", new Date(11,10,1991), hire, 1000, 0.25,300);
         //System.out.println(e5.getEarnings());
         
         Employee[] employees = new Employee[3];
         employees[0] =  new SalaryCommissionEmployee("Jim", "Frankie", "123-45-6789", new Date(11,10,1991), hire, 1000, 0.25,300);
         employees[1] =  new CommissionEmployee("Gary", "Hen", "123457387", new Date(11,10,1972), hire, 100000, 0.50);
         employees[2] =  new SalaryEmployee("Kelly", "Lowe", "3268762834", new Date(1,1,2100), hire, 3000000);
         for (Employee e : employees)
         {
             System.out.println(e);
             System.out.println(e.getEarnings());
         }
         
    }
}