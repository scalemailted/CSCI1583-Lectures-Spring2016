public class CommissionSalaryEmployee
{
    CommissionEmployee commissionEmployee;
    double monthlySalary;
    
    public CommissionSalaryEmployee(String firstName, String lastName, 
                                    String ssn, Date dob, Date hireDate, 
                                    double grossSales, double commissionRate,
                                    double monthlySalary)
    {
        this.commissionEmployee = new CommissionEmployee(firstName, lastName, ssn, dob, hireDate, grossSales, commissionRate);
        this.monthlySalary = monthlySalary;
    }
    
    public double getEarnings()
    {
        this.commissionEmployee.getEarnings() + this.monthlySalary/2;
    }
    
    public String getFirstName()
    {
        return this.commissionEmployee.getFirstName();
    }
}
